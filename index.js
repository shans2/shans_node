require('dotenv').config({ path: '/app/.env' })

const mongooseConnection = require("./config/mongooseConnection");
const grpcConnection = require("./config/grpcConnection");