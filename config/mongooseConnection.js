const cfg = require('./index')
const mongoose = require('mongoose')
const logger = require('./logger')

let mongoDBUrl = 
    "mongodb://" +
    cfg.mongoUser +
    ":" +
    cfg.mongoPassword +
    "@" +
    cfg.mongoHost +
    ":" +
    cfg.mongoPort +
    "/" +
    cfg.mongoDatabase;
mongoose.connect(
    mongoDBUrl,
    {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        // useCreateIndex: true,
    },
    (err) => {
        if (err) {
            console.log("Error while connecting to database (" + 
            mongoDBUrl + ") "+ err.message);
            logger.error("Error while connecting to database (" + 
            mongoDBUrl + ") "+ err.message);
        }

        logger.info("Connected to mongoDB")
    }
);
