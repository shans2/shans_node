const protoLoader = require("@grpc/proto-loader");
const grpc = require("@grpc/grpc-js");

const cfg = require('./index')
const logger = require('./logger')

// const branchService = require("../services/branch");
const divisionService = require("../services/division");
const categoryService = require("../services/category");
// const subcategoryService = require("../services/subcategory");
// const brandService = require("../services/brand");
// const attributeService = require("../services/attribute");
// const videoService = require("../services/video");
// const deliveryMeasureService = require("../services/delivery_measure");
// const stickerService = require("../services/sticker");
// const bannerService = require("../services/banner");
// const discountBannerService = require("../services/discount_banner");
// const voucherService = require("../services/voucher");
// const ribbonService = require("../services/ribbon");
// const discountService = require("../services/discount");
// const groupService = require("../services/group");
// const promotionService = require("../services/promotion");


const PROTO_URL = __dirname + "/../protos/content_service/content_service.proto";
const packageDefinition = protoLoader.loadSync(PROTO_URL, {
    keepCase: true,
    longs: String,
    enums: String,
    defaults: true,
    oneofs: true
});

const contentProto =
    grpc.loadPackageDefinition(packageDefinition).content_service;

var server = new grpc.Server();

server.addService(contentProto.BranchService.service, branchService);
server.addService(contentProto.DivisionService.service, divisionService);
server.addService(contentProto.CategoryService.service, categoryService);
server.addService(contentProto.SubcategoryService.service, subcategoryService);
server.addService(contentProto.BrandService.service, brandService);
server.addService(contentProto.AttributeService.service, attributeService);
server.addService(contentProto.VideoService.service, videoService);
server.addService(contentProto.DeliveryMeasureService.service, deliveryMeasureService);
server.addService(contentProto.StickerService.service, stickerService);
server.addService(contentProto.BannerService.service, bannerService);
server.addService(contentProto.DiscountBannerService.service, discountBannerService);
server.addService(contentProto.VoucherService.service, voucherService);
server.addService(contentProto.RibbonService.service, ribbonService);
server.addService(contentProto.DiscountService.service, discountService);
server.addService(contentProto.GroupService.service, groupService);
server.addService(contentProto.PromotionService.service, promotionService);
server.addService(contentProto.ContactService.service, contactService);

server.bindAsync(
    "0.0.0.0:" + cfg.RPCPort,
    grpc.ServerCredentials.createInsecure(),
    (err, bindPort) => {
        if (err) {
            throw new Error("Error while binding grpc server to the port");
        }

        logger.info("grpc server is running at %s", bindPort);
        server.start();
    }
);
