const config = {
    environment: getConf("NODE_ENV", "dev"),
    mongoHost: getConf("MONGO_HOST", "localhost"),
    mongoPort: getConf("MONGO_PORT", "27017"),
    mongoUser: getConf("MONGO_USER", "admin"),
    mongoPassword: getConf("MONGO_PASSWORD", "admin1234"),
    mongoDatabase: getConf("MONGO_DATABASE", "mb_content_service"),
    RPCPort: getConf("RPC_PORT", 8005),
    limit: getConf("DEFAULT_LIMIT", 10),
    page: getConf("DEFAULT_PAGE", 1)
};

function getConf(name, def = "") {
    if (process.env[name]) {
        return process.env[name];
    }
    return def;
}

module.exports = config;
