const bannerStore = require("../storage/mongo/banner");
const catchWrapService = require("../helper/catchWrapService");

const bannerService = {
    Create: catchWrapService(`service.banner.create`, bannerStore.create),
    Update: catchWrapService(`service.banner.update`, bannerStore.update),
    GetSingle: catchWrapService(`service.banner.getSingle`, bannerStore.getSingle),
    GetList: catchWrapService(`service.banner.getList`, bannerStore.getList),
    Delete: catchWrapService(`service.banner.delete`, bannerStore.delete),
};

module.exports = bannerService;
