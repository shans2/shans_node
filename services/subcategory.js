const subcategoryStore = require("../storage/mongo/subcategory");
const catchWrapService = require("../helper/catchWrapService");

const subcategoryService = {
    Create: catchWrapService(`service.subcategory.create`, subcategoryStore.create),
    Update: catchWrapService(`service.subcategory.update`, subcategoryStore.update),
    GetSingle: catchWrapService(`service.subcategory.getSingle`, subcategoryStore.getSingle),
    GetList: catchWrapService(`service.subcategory.getList`, subcategoryStore.getList),
    Delete: catchWrapService(`service.subcategory.delete`, subcategoryStore.delete),
};

module.exports = subcategoryService;