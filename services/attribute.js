const attributeStore = require("../storage/mongo/attribute");
const catchWrapService = require("../helper/catchWrapService");

const attributeService = {
    Create: catchWrapService(`service.attribute.create`, attributeStore.create),
    Update: catchWrapService(`service.attribute.update`, attributeStore.update),
    GetSingle: catchWrapService(`service.attribute.getSingle`, attributeStore.getSingle),
    GetList: catchWrapService(`service.attribute.getList`, attributeStore.getList),
    Delete: catchWrapService(`service.attribute.delete`, attributeStore.delete),
};

module.exports = attributeService;
