const stickerStore = require("../storage/mongo/sticker");
const catchWrapService = require("../helper/catchWrapService");

const stickerService = {
    Create: catchWrapService(`service.sticker.create`, stickerStore.create),
    Update: catchWrapService(`service.sticker.update`, stickerStore.update),
    GetSingle: catchWrapService(`service.sticker.getSingle`, stickerStore.getSingle),
    GetList: catchWrapService(`service.sticker.getList`, stickerStore.getList),
    Delete: catchWrapService(`service.sticker.delete`, stickerStore.delete),
};

module.exports = stickerService;
