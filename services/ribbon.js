const ribbonStore = require("../storage/mongo/ribbon");
const catchWrapService = require("../helper/catchWrapService");

const ribbonService = {
    Create: catchWrapService(`service.ribbon.create`, ribbonStore.create),
    Update: catchWrapService(`service.ribbon.update`, ribbonStore.update),
    GetSingle: catchWrapService(`service.ribbon.getSingle`, ribbonStore.getSingle),
    GetList: catchWrapService(`service.ribbon.getList`, ribbonStore.getList),
    Delete: catchWrapService(`service.ribbon.delete`, ribbonStore.delete),
};

module.exports = ribbonService;
