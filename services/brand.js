const brandStore = require("../storage/mongo/brand");
const catchWrapService = require("../helper/catchWrapService");

const brandService = {
    Create: catchWrapService(`service.brand.create`, brandStore.create),
    Update: catchWrapService(`service.brand.update`, brandStore.update),
    GetSingle: catchWrapService(`service.brand.getSingle`, brandStore.getSingle),
    GetList: catchWrapService(`service.brand.getList`, brandStore.getList),
    Delete: catchWrapService(`service.brand.delete`, brandStore.delete),
};

module.exports = brandService;
