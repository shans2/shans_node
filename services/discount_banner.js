const discountBannerStore = require("../storage/mongo/discount_banner");
const catchWrapService = require("../helper/catchWrapService");

const discountBannerService = {
    Create: catchWrapService(`service.discount_banner.create`, discountBannerStore.create),
    Update: catchWrapService(`service.discount_banner.update`, discountBannerStore.update),
    GetSingle: catchWrapService(`service.discount_banner.getSingle`, discountBannerStore.getSingle),
    GetList: catchWrapService(`service.discount_banner.getList`, discountBannerStore.getList),
    Delete: catchWrapService(`service.discount_banner.delete`, discountBannerStore.delete),
};

module.exports = discountBannerService;
