const voucherStore = require("../storage/mongo/voucher");
const catchWrapService = require("../helper/catchWrapService");

const voucherService = {
    Create: catchWrapService(`service.voucher.create`, voucherStore.create),
    Update: catchWrapService(`service.voucher.update`, voucherStore.update),
    GetSingle: catchWrapService(`service.voucher.getSingle`, voucherStore.getSingle),
    GetList: catchWrapService(`service.voucher.getList`, voucherStore.getList),
    Delete: catchWrapService(`service.voucher.delete`, voucherStore.delete),
};

module.exports = voucherService;
