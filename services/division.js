const divisionStore = require("../storage/mongo/division");
const catchWrapService = require("../helper/catchWrapService");

const divisionService = {
    Create: catchWrapService(`service.division.create`, divisionStore.create),
    Update: catchWrapService(`service.division.update`, divisionStore.update),
    GetSingle: catchWrapService(`service.division.getSingle`, divisionStore.getSingle),
    GetList: catchWrapService(`service.division.getList`, divisionStore.getList),
    Delete: catchWrapService(`service.division.delete`, divisionStore.delete),
};

module.exports = divisionService;
