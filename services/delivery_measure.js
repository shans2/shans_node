const deliveryMeasureStore = require("../storage/mongo/delivery_measure");
const catchWrapService = require("../helper/catchWrapService");

const deliveryMeasureService = {
    Create: catchWrapService(`service.delivery_measure.create`, deliveryMeasureStore.create),
    Update: catchWrapService(`service.delivery_measure.update`, deliveryMeasureStore.update),
    GetSingle: catchWrapService(`service.delivery_measure.getSingle`, deliveryMeasureStore.getSingle),
    GetList: catchWrapService(`service.delivery_measure.getList`, deliveryMeasureStore.getList),
    Delete: catchWrapService(`service.delivery_measure.delete`, deliveryMeasureStore.delete),
};

module.exports = deliveryMeasureService;
