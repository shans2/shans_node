const groupStore = require("../storage/mongo/group");
const catchWrapService = require("../helper/catchWrapService");

const groupService = {
    Create: catchWrapService(`service.group.create`, groupStore.create),
    Update: catchWrapService(`service.group.update`, groupStore.update),
    GetSingle: catchWrapService(`service.group.getSingle`, groupStore.getSingle),
    GetList: catchWrapService(`service.group.getList`, groupStore.getList),
    Delete: catchWrapService(`service.group.delete`, groupStore.delete),
};

module.exports = groupService;
