const branchStore = require("../storage/mongo/branch");
const catchWrapService = require("../helper/catchWrapService");

const branchService = {
    Create: catchWrapService(`service.branch.create`, branchStore.create),
    Update: catchWrapService(`service.branch.update`, branchStore.update),
    GetSingle: catchWrapService(`service.branch.getSingle`, branchStore.getSingle),
    GetList: catchWrapService(`service.branch.getList`, branchStore.getList),
    Delete: catchWrapService(`service.branch.delete`, branchStore.delete),
};

module.exports = branchService;
