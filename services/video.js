const videoStore = require("../storage/mongo/video");
const catchWrapService = require("../helper/catchWrapService");

const videoService = {
    Create: catchWrapService(`service.video.create`, videoStore.create),
    Update: catchWrapService(`service.video.update`, videoStore.update),
    GetSingle: catchWrapService(`service.video.getSingle`, videoStore.getSingle),
    GetList: catchWrapService(`service.video.getList`, videoStore.getList),
    Delete: catchWrapService(`service.video.delete`, videoStore.delete),
};

module.exports = videoService;
