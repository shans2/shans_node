const categoryStore = require("../storage/mongo/category");
const catchWrapService = require("../helper/catchWrapService");

const categoryService = {
    Create: catchWrapService(`service.category.create`, categoryStore.create),
    Update: catchWrapService(`service.category.update`, categoryStore.update),
    GetSingle: catchWrapService(`service.category.getSingle`, categoryStore.getSingle),
    GetList: catchWrapService(`service.category.getList`, categoryStore.getList),
    Delete: catchWrapService(`service.category.delete`, categoryStore.delete),
};

module.exports = categoryService;
