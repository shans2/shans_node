const promotionStore = require("../storage/mongo/promotion");
const catchWrapService = require("../helper/catchWrapService");

const promotionService = {
    Create: catchWrapService(`service.promotion.create`, promotionStore.create),
    Update: catchWrapService(`service.promotion.update`, promotionStore.update),
    GetSingle: catchWrapService(`service.promotion.getSingle`, promotionStore.getSingle),
    GetList: catchWrapService(`service.promotion.getList`, promotionStore.getList),
    Delete: catchWrapService(`service.promotion.delete`, promotionStore.delete),
};

module.exports = promotionService;
