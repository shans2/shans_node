const discountStore = require("../storage/mongo/discount");
const catchWrapService = require("../helper/catchWrapService");

const discountService = {
    Create: catchWrapService(`service.discount.create`, discountStore.create),
    Update: catchWrapService(`service.discount.update`, discountStore.update),
    GetSingle: catchWrapService(`service.discount.getSingle`, discountStore.getSingle),
    GetList: catchWrapService(`service.discount.getList`, discountStore.getList),
    Delete: catchWrapService(`service.discount.delete`, discountStore.delete),
};

module.exports = discountService;
