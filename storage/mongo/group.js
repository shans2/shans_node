const Group = require("../../models/group");
const catchWrapDb = require("../../helper/catchWrapDb");


let NAMESPACE = "storage.group";

let groupStore = {
    create: catchWrapDb(`${NAMESPACE}.create`, async(data) => {
        const group = new Group(data);

        const response = await group.save();

        return response;
    }),
    update: catchWrapDb(`${NAMESPACE}.update`, async(data) => {
    
        const group = await Group.updateOne(
            {
                id: data.id,
            },
            {
                $set: data
            }
        )
        
        return group;
    }),
    getList: catchWrapDb(`${NAMESPACE}.getList`, async(data) => {        
        let query = {}
        if (data.name) {
            query["name"] = data.name
        }
        if (data.status) {
            query["status"] = data.status
        }
        const groups = await Group.find(
            query,
            null,
            {
                sort: {num_id: -1}
            }
        ).skip(data.offset)
        .limit(data.limit);

        const count = await Group.countDocuments(query);
        
        return {groups, count};
    }
    ),
    getSingle: catchWrapDb(`${NAMESPACE}.getSingle`, async (data) => {
        const group = await Group.findOne({id: data.id});

        return group;
    }),
    delete: catchWrapDb(`${NAMESPACE}.delete`, async(data) => {
        const group = await Group.deleteOne({id: data.id});

        return group;
    }
    ),
};

module.exports = groupStore;
