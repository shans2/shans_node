const Sticker = require("../../models/sticker");
const catchWrapDb = require("../../helper/catchWrapDb");


let NAMESPACE = "storage.sticker";

let stickerStore = {
    create: catchWrapDb(`${NAMESPACE}.create`, async(data) => {
        const sticker = new Sticker(data);

        const response = await sticker.save();

        return response;
    }),
    update: catchWrapDb(`${NAMESPACE}.update`, async(data) => {
        const sticker = await Sticker.updateOne(
            {
                id: data.id,
            },
            {
                $set: data
            }
        )
        
        return sticker;
    }),
    getList: catchWrapDb(`${NAMESPACE}.getList`, async(data) => {        
        let query = {}
        if (data.name) {
            query["$or"] =  [{ "name.ru": RegExp(data.name,"i")}, { "name.uz": RegExp(data.name,"i") }, { "name.kr": RegExp(data.name,"i") }]
        }
        if (data.status) {
            query["status"] = data.status
        }
        const stickers = await Sticker.find(
            query,
            null,
            {
                sort: {num_id: -1}
            }
        ).skip(data.offset)
        .limit(data.limit);

        const count = await Sticker.countDocuments(query);
        
        return {stickers, count};
    }
    ),
    getSingle: catchWrapDb(`${NAMESPACE}.getSingle`, async (data) => {
        const sticker = await Sticker.findOne({id: data.id});

        return sticker;
    }),
    delete: catchWrapDb(`${NAMESPACE}.delete`, async(data) => {
        const sticker = await Sticker.deleteOne({id: data.id});

        return sticker;
    }
    ),
};

module.exports = stickerStore;
