const Video = require("../../models/video");
const catchWrapDb = require("../../helper/catchWrapDb");


let NAMESPACE = "storage.video";

let videoStore = {
    create: catchWrapDb(`${NAMESPACE}.create`, async(data) => {
        const video = new Video(data);

        const response = await video.save();

        return response;
    }),
    update: catchWrapDb(`${NAMESPACE}.update`, async(data) => {
    
        const video = await Video.updateOne(
            {
                id: data.id,
            },
            {
                $set: data
            }
        )
        
        return video;
    }),
    getList: catchWrapDb(`${NAMESPACE}.getList`, async(data) => {        
        let query = {}
        
        if (data.name) {
            query["$or"] =  [{ "name.ru": RegExp(data.name,"i")}, { "name.uz": RegExp(data.name,"i") }, { "name.kr": RegExp(data.name,"i") }]
        }

        if (data.status) {
            query["status"] = data.status
        }

        const videos = await Video.find(
            query,
            null,
            {
                sort: {num_id: -1}
            }
        ).skip(data.offset)
        .limit(data.limit);

        const count = await Video.countDocuments(query);
        
        return {videos, count};
    }
    ),
    getSingle: catchWrapDb(`${NAMESPACE}.getSingle`, async (data) => {
        const video = await Video.findOne({id: data.id});

        return video;
    }),
    delete: catchWrapDb(`${NAMESPACE}.delete`, async(data) => {
        const video = await Video.deleteOne({id: data.id});

        return video;
    }
    ),
};

module.exports = videoStore;
