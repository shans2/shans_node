const Banner = require("../../models/banner");
const catchWrapDb = require("../../helper/catchWrapDb");


let NAMESPACE = "storage.banner";

let bannerStore = {
    create: catchWrapDb(`${NAMESPACE}.create`, async(data) => {
        const banner = new Banner(data);

        const response = await banner.save();

        return response;
    }),
    update: catchWrapDb(`${NAMESPACE}.update`, async(data) => {
    
        const banner = await Banner.updateOne(
            {
                id: data.id,
            },
            {
                $set: data
            }
        )
        
        return banner;
    }),
    getList: catchWrapDb(`${NAMESPACE}.getList`, async(data) => {        
        let query = {}
        if (data.name) {
            query["$or"] =  [{ "name.ru": RegExp(data.name,"i")}, { "name.uz": RegExp(data.name,"i") }, { "name.kr": RegExp(data.name,"i") }]
        }
        if (data.status) {
            query["status"] = data.status
        }
        if (data.type) {
            query["type"] = data.type
        }
        const banners = await Banner.find(
            query,
            null,
            {
                sort: {num_id: -1}
            }
        ).skip(data.offset)
        .limit(data.limit);

        const count = await Banner.countDocuments(query);
        
        return {banners, count};
    }
    ),
    getSingle: catchWrapDb(`${NAMESPACE}.getSingle`, async (data) => {
        const banner = await Banner.findOne({id: data.id});

        return banner;
    }),
    delete: catchWrapDb(`${NAMESPACE}.delete`, async(data) => {
        const banner = await Banner.deleteOne({id: data.id});

        return banner;
    }
    ),
};

module.exports = bannerStore;
