const Attribute = require("../../models/attribute");
const catchWrapDb = require("../../helper/catchWrapDb");


let NAMESPACE = "storage.attribute";

let attributeStore = {
    create: catchWrapDb(`${NAMESPACE}.create`, async(data) => {
        const attribute = new Attribute(data);

        const response = await attribute.save();

        return response;
    }),
    update: catchWrapDb(`${NAMESPACE}.update`, async(data) => {
    
        const attribute = await Attribute.updateOne(
            {
                id: data.id,
            },
            {
                $set: data
            }
        )
        
        return attribute;
    }),
    getList: catchWrapDb(`${NAMESPACE}.getList`, async(data) => {        
        let query = {}
        if (data.name) {
            query["$or"] =  [{ "name.ru": RegExp(data.name,"i")}, { "name.uz": RegExp(data.name,"i") }, { "name.kr": RegExp(data.name,"i") }]
        }
        if (data.status) {
            query["status"] = data.status
        }

        if (data.group_id) {
            query["group_id"] = data.group_id
        }
        const attributes = await Attribute.find(
            query,
            null,
            {
                sort: {num_id: -1}
            }
        ).skip(data.offset)
        .limit(data.limit)
        .populate("group");

        const count = await Attribute.countDocuments(query);
        
        return {attributes, count};
    }
    ),
    getSingle: catchWrapDb(`${NAMESPACE}.getSingle`, async (data) => {
        const attribute = await Attribute.findOne({id: data.id}).populate("group");

        return attribute;
    }),
    delete: catchWrapDb(`${NAMESPACE}.delete`, async(data) => {
        const attribute = await Attribute.deleteOne({id: data.id});

        return attribute;
    }
    ),
};

module.exports = attributeStore;
