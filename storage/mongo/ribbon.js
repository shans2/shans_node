const Ribbon = require("../../models/ribbon");
const catchWrapDb = require("../../helper/catchWrapDb");


let NAMESPACE = "storage.ribbon";

let ribbonStore = {
    create: catchWrapDb(`${NAMESPACE}.create`, async(data) => {
        const ribbon = new Ribbon(data);

        const response = await ribbon.save();

        return response;
    }),
    update: catchWrapDb(`${NAMESPACE}.update`, async(data) => {
    
        const ribbon = await Ribbon.updateOne(
            {
                id: data.id,
            },
            {
                $set: data
            }
        )
        
        return ribbon;
    }),
    getList: catchWrapDb(`${NAMESPACE}.getList`, async(data) => {        
        let query = {}
        if (data.name) {
            query["$or"] =  [{ "name.ru": RegExp(data.name,"i")}, { "name.uz": RegExp(data.name,"i") }, { "name.kr": RegExp(data.name,"i") }]
        }
        if (data.status) {
            query["status"] = data.status
        }
        const ribbons = await Ribbon.find(
            query,
            null,
            {
                sort: {num_id: -1}
            }
        ).skip(data.offset)
        .limit(data.limit);

        const count = await Ribbon.countDocuments(query);
        
        return {ribbons, count};
    }
    ),
    getSingle: catchWrapDb(`${NAMESPACE}.getSingle`, async (data) => {
        const ribbon = await Ribbon.findOne({id: data.id});

        return ribbon;
    }),
    delete: catchWrapDb(`${NAMESPACE}.delete`, async(data) => {
        const ribbon = await Ribbon.deleteOne({id: data.id});

        return ribbon;
    }
    ),
};

module.exports = ribbonStore;
