const Division = require("../../models/division");
const catchWrapDb = require("../../helper/catchWrapDb");

let NAMESPACE = "storage.division";

let divisionStore = {
    create: catchWrapDb(`${NAMESPACE}.create`, async(data) => {
        const division = new Division(data);

        const response = await division.save();

        return response;
    }),
    update: catchWrapDb(`${NAMESPACE}.update`, async(data) => {
    
        const division = await Division.updateOne(
            {
                id: data.id,
            },
            {
                $set: data
            }
        )
        
        return division;
    }),
    getList: catchWrapDb(`${NAMESPACE}.getList`, async(data) => {        
        let query = {}
        if (data.name) {
            query["$or"] =  [{ "name.ru": RegExp(data.name,"i")}, { "name.uz": RegExp(data.name,"i") }, { "name.kr": RegExp(data.name,"i") }]
        }
        if (data.status) {
            query["status"] = data.status
        }
        const divisions = await Division.find(
            query,
            null,
            {
                sort: {num_id: -1}
            }
        ).skip(data.offset)
        .limit(data.limit);

        const count = await Division.countDocuments(query);
        
        return {divisions, count};
    }
    ),
    getSingle: catchWrapDb(`${NAMESPACE}.getSingle`, async (data) => {
        const division = await Division.findOne({id: data.id});

        return division;
    }),
    delete: catchWrapDb(`${NAMESPACE}.delete`, async(data) => {
        const division = await Division.deleteOne({id: data.id});

        return division;
    }
    ),
};

module.exports = divisionStore;
