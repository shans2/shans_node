const Promotion = require("../../models/promotion");
const catchWrapDb = require("../../helper/catchWrapDb");


let NAMESPACE = "storage.promotion";

let promotionStore = {
    create: catchWrapDb(`${NAMESPACE}.create`, async(data) => {
        const promotion = new Promotion(data);

        const response = await promotion.save();

        return response;
    }),
    update: catchWrapDb(`${NAMESPACE}.update`, async(data) => {
    
        const promotion = await Promotion.updateOne(
            {
                id: data.id,
            },
            {
                $set: data
            }
        )
        
        return promotion;
    }),
    getList: catchWrapDb(`${NAMESPACE}.getList`, async(data) => {        
        let query = {}
        if (data.name) {
            query["$or"] =  [{ "name.ru": RegExp(data.name,"i")}, { "name.uz": RegExp(data.name,"i") }, { "name.kr": RegExp(data.name,"i") }]
        }
        if (data.status) {
            query["status"] = data.status
        }
        const promotions = await Promotion.find(
            query,
            null,
            {
                sort: {num_id: -1}
            }
        ).skip(data.offset)
        .limit(data.limit);

        const count = await Promotion.countDocuments(query);
        
        return {promotions, count};
    }
    ),
    getSingle: catchWrapDb(`${NAMESPACE}.getSingle`, async (data) => {
        const promotion = await Promotion.findOne({id: data.id});

        return promotion;
    }),
    delete: catchWrapDb(`${NAMESPACE}.delete`, async(data) => {
        const promotion = await Promotion.deleteOne({id: data.id});

        return promotion;
    }
    ),
};

module.exports = promotionStore;
