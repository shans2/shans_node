const Discount = require("../../models/discount");
const catchWrapDb = require("../../helper/catchWrapDb");


let NAMESPACE = "storage.discount";

let discountStore = {
    create: catchWrapDb(`${NAMESPACE}.create`, async(data) => {
        const discount = new Discount(data);

        const response = await discount.save();

        return response;
    }),
    update: catchWrapDb(`${NAMESPACE}.update`, async(data) => {
    
        const discount = await Discount.updateOne(
            {
                id: data.id,
            },
            {
                $set: data
            }
        )
        
        return discount;
    }),
    getList: catchWrapDb(`${NAMESPACE}.getList`, async(data) => {        
        let query = {}
        if (data.name) {
            query["$or"] =  [{ "name.ru": RegExp(data.name,"i")}, { "name.uz": RegExp(data.name,"i") }, { "name.kr": RegExp(data.name,"i") }]
        }
        if (data.status) {
            query["status"] = data.status
        }
        const discounts = await Discount.find(
            query,
            null,
            {
                sort: {num_id: -1}
            }
        ).skip(data.offset)
        .limit(data.limit);

        const count = await Discount.countDocuments(query);
        
        return {discounts, count};
    }
    ),
    getSingle: catchWrapDb(`${NAMESPACE}.getSingle`, async (data) => {
        const discount = await Discount.findOne({id: data.id});

        return discount;
    }),
    delete: catchWrapDb(`${NAMESPACE}.delete`, async(data) => {
        const discount = await Discount.deleteOne({id: data.id});

        return discount;
    }
    ),
};

module.exports = discountStore;
