const Branch = require("../../models/branch");
const catchWrapDb = require("../../helper/catchWrapDb");


let NAMESPACE = "storage.branch";

let branchStore = {
    create: catchWrapDb(`${NAMESPACE}.create`, async(data) => {
        const branch = new Branch(data);

        const response = await branch.save();

        return response;
    }),
    update: catchWrapDb(`${NAMESPACE}.update`, async(data) => {
    
        const branch = await Branch.updateOne(
            {
                id: data.id,
            },
            {
                $set: data
            }
        )
        
        return branch;
    }),
    getList: catchWrapDb(`${NAMESPACE}.getList`, async(data) => {    
        let query = {}    
        if (data.name) {
            query["$or"] =  [{ "name.ru": RegExp(data.name,"i")}, { "name.uz": RegExp(data.name,"i") }, { "name.kr": RegExp(data.name,"i") }]
        }
        if (data.status) {
            query["status"] = data.status
        }
        const branches = await Branch.find(
            query,
            null,
            {
                sort: {num_id: -1}
            }
        ).skip(data.offset)
        .limit(data.limit);

        const count = await Branch.countDocuments(query);
        return {branches, count};
    }
    ),
    getSingle: catchWrapDb(`${NAMESPACE}.getSingle`, async (data) => {
        const branch = await Branch.findOne({id: data.id});

        return branch;
    }),
    delete: catchWrapDb(`${NAMESPACE}.delete`, async(data) => {
        const branch = await Branch.deleteOne({id: data.id});

        return branch;
    }
    ),
};

module.exports = branchStore;
