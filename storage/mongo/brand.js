const Brand = require("../../models/brand");
const catchWrapDb = require("../../helper/catchWrapDb");

let NAMESPACE = "storage.brand";

let brandStore = {
    create: catchWrapDb(`${NAMESPACE}.create`, async(data) => {
        const brand = new Brand(data);

        const response = await brand.save();

        return response;
    }),
    update: catchWrapDb(`${NAMESPACE}.update`, async(data) => {
    
        const brand = await Brand.updateOne(
            {
                id: data.id,
            },
            {
                $set: data
            }
        )
        
        return brand;
    }),
    getList: catchWrapDb(`${NAMESPACE}.getList`, async(data) => {        
        let query = {}
        if (data.name) {
            query["$or"] =  [{ "name.ru": RegExp(data.name,"i")}, { "name.uz": RegExp(data.name,"i") }, { "name.kr": RegExp(data.name,"i") }]
        }
        if (data.status) {
            query["status"] = data.status
        }
        const brands = await Brand.find(
            query,
            null,
            {
                sort: {num_id: -1}
            }
        ).skip(data.offset)
        .limit(data.limit);

        const count = await Brand.countDocuments(query);
        
        return {brands, count};
    }
    ),
    getSingle: catchWrapDb(`${NAMESPACE}.getSingle`, async (data) => {
        const brand = await Brand.findOne({id: data.id});

        return brand;
    }),
    delete: catchWrapDb(`${NAMESPACE}.delete`, async(data) => {
        const brand = await Brand.deleteOne({id: data.id});

        return brand;
    }
    ),
};

module.exports = brandStore;
