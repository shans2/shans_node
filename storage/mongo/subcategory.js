const Subcategory = require("../../models/subcategory");
const catchWrapDb = require("../../helper/catchWrapDb");


let NAMESPACE = "storage.subcategory";

let subcategoryStore = {
    create: catchWrapDb(`${NAMESPACE}.create`, async(data) => {
        const subcategory = new Subcategory(data);

        const response = await subcategory.save();

        return response;
    }),
    update: catchWrapDb(`${NAMESPACE}.update`, async(data) => {
    
        const subcategory = await Subcategory.updateOne(
            {
                id: data.id,
            },
            {
                $set: data
            }
        )
        
        return subcategory;
    }),
    getList: catchWrapDb(`${NAMESPACE}.getList`, async(data) => {        
        let query = {}
        if (data.name) {
            query["$or"] =  [{ "name.ru": RegExp(data.name,"i")}, { "name.uz": RegExp(data.name,"i") }, { "name.kr": RegExp(data.name,"i") }]
        }
        if (data.category_id) {
            query.category_id = data.category_id
        }
        if (data.order) {
            query.order = data.order
        }
        if (data.status) {
            query["status"] = data.status
        }
        
        const subcategories = await Subcategory.find(
            query,
            null,
            {
                sort: {num_id: -1}
            }
        ).skip(data.offset)
        .limit(data.limit)
        .populate("category");

        const count = await Subcategory.countDocuments(query);
        return {subcategories, count};
    }
    ),
    getSingle: catchWrapDb(`${NAMESPACE}.getSingle`, async (data) => {
        const subcategory = await Subcategory.findOne({id: data.id}).populate("category");

        return subcategory;
    }),
    delete: catchWrapDb(`${NAMESPACE}.delete`, async(data) => {
        const subcategory = await Subcategory.deleteOne({id: data.id});

        return subcategory;
    }
    ),
};

module.exports = subcategoryStore;
