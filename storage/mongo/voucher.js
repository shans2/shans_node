const Voucher = require("../../models/voucher");
const catchWrapDb = require("../../helper/catchWrapDb");


let NAMESPACE = "storage.voucher";

let voucherStore = {
    create: catchWrapDb(`${NAMESPACE}.create`, async(data) => {
        const voucher = new Voucher(data);

        const response = await voucher.save();

        return response;
    }),
    update: catchWrapDb(`${NAMESPACE}.update`, async(data) => {
    
        const voucher = await Voucher.updateOne(
            {
                id: data.id,
            },
            {
                $set: data
            }
        )
        
        return voucher;
    }),
    getList: catchWrapDb(`${NAMESPACE}.getList`, async(data) => {        
        let query = {}
        if (data.name) {
            query["$or"] =  [{ "name.ru": RegExp(data.name,"i")}, { "name.uz": RegExp(data.name,"i") }, { "name.kr": RegExp(data.name,"i") }]
        }
        if (data.status) {
            query["status"] = data.status
        }
        const vouchers = await Voucher.find(
            query,
            null,
            {
                sort: {num_id: -1}
            }
        ).skip(data.offset)
        .limit(data.limit);

        const count = await Voucher.countDocuments(query);
        
        return {vouchers, count};
    }
    ),
    getSingle: catchWrapDb(`${NAMESPACE}.getSingle`, async (data) => {
        const voucher = await Voucher.findOne({id: data.id});

        return voucher;
    }),
    delete: catchWrapDb(`${NAMESPACE}.delete`, async(data) => {
        const voucher = await Voucher.deleteOne({id: data.id});

        return voucher;
    }
    ),
};

module.exports = voucherStore;
