const DiscountBanner = require("../../models/discount_banner");
const catchWrapDb = require("../../helper/catchWrapDb");


let NAMESPACE = "storage.discount_banner";

let discountBannerStore = {
    create: catchWrapDb(`${NAMESPACE}.create`, async(data) => {
        const discount_banner = new DiscountBanner(data);

        const response = await discount_banner.save();

        return response;
    }),
    update: catchWrapDb(`${NAMESPACE}.update`, async(data) => {
    
        const discount_banner = await DiscountBanner.updateOne(
            {
                id: data.id,
            },
            {
                $set: data
            }
        )
        
        return discount_banner;
    }),
    getList: catchWrapDb(`${NAMESPACE}.getList`, async(data) => {        
        let query = {}
        if (data.name) {
            query["$or"] =  [{ "name.ru": RegExp(data.name,"i")}, { "name.uz": RegExp(data.name,"i") }, { "name.kr": RegExp(data.name,"i") }]
        }
        if (data.status) {
            query["status"] = data.status
        }
        const discount_banners = await DiscountBanner.find(
            query,
            null,
            {
                sort: {num_id: -1}
            }
        ).skip(data.offset)
        .limit(data.limit);

        const count = await DiscountBanner.countDocuments(query);
        
        return {discount_banners, count};
    }
    ),
    getSingle: catchWrapDb(`${NAMESPACE}.getSingle`, async (data) => {
        const discount_banner = await DiscountBanner.findOne({id: data.id});

        return discount_banner;
    }),
    delete: catchWrapDb(`${NAMESPACE}.delete`, async(data) => {
        const discount_banner = await DiscountBanner.deleteOne({id: data.id});

        return discount_banner;
    }
    ),
};

module.exports = discountBannerStore;
