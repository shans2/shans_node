const Category = require("../../models/category");
const catchWrapDb = require("../../helper/catchWrapDb");

let NAMESPACE = "storage.category";

let categoryStore = {
    create: catchWrapDb(`${NAMESPACE}.create`, async(data) => {
        const category = new Category(data);

        const response = await category.save();

        return response;
    }),
    update: catchWrapDb(`${NAMESPACE}.update`, async(data) => {
    
        const category = await Category.updateOne(
            {
                id: data.id,
            },
            {
                $set: data
            }
        )
        
        return category;
    }),
    getList: catchWrapDb(`${NAMESPACE}.getList`, async(data) => {        
        let query = {}
        if (data.name) {
            query["$or"] =  [{ "name.ru": RegExp(data.name,"i")}, { "name.uz": RegExp(data.name,"i") }, { "name.kr": RegExp(data.name,"i") }]
        }
        if (data.division_id) {
            query.division_id = data.division_id
        }
        if (data.order) {
            query.order = data.order
        }
        if (data.status) {
            query["status"] = data.status
        }

        const categories = await Category.find(
            query,
            null,
            {
                sort: {num_id: -1}
            }
        ).skip(data.offset)
        .limit(data.limit)
        .populate("division");

        const count = await Category.countDocuments(query);
        
        return {categories, count};
    }
    ),
    getSingle: catchWrapDb(`${NAMESPACE}.getSingle`, async (data) => {
        const category = await Category.findOne({id: data.id}).populate("division");

        return category;
    }),
    delete: catchWrapDb(`${NAMESPACE}.delete`, async(data) => {
        const category = await Category.deleteOne({id: data.id});

        return category;
    }
    ),
};

module.exports = categoryStore;
