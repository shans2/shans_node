const DeliveryMeasure = require("../../models/delivery_measure");
const catchWrapDb = require("../../helper/catchWrapDb");


let NAMESPACE = "storage.delivery_measure";

let deliveryMeasureStore = {
    create: catchWrapDb(`${NAMESPACE}.create`, async(data) => {
        const deliveryMeasure = new DeliveryMeasure(data);

        const response = await deliveryMeasure.save();

        return response;
    }),
    update: catchWrapDb(`${NAMESPACE}.update`, async(data) => {
    
        const deliveryMeasure = await DeliveryMeasure.updateOne(
            {
                id: data.id,
            },
            {
                $set: data
            }
        )
        
        return deliveryMeasure;
    }),
    getList: catchWrapDb(`${NAMESPACE}.getList`, async(data) => {        
        let query = {}
        if (data.region) {
            query["$or"] =  [{ "region.ru": RegExp(data.region,"i")}, { "region.uz": RegExp(data.region,"i") }, { "region.kr": RegExp(data.region,"i") }]
        }
        if (data.status) {
            query["status"] = data.status
        }
        const delivery_measures = await DeliveryMeasure.find(
            query,
            null,
            {
                sort: {num_id: -1}
            }
        ).skip(data.offset)
        .limit(data.limit);

        const count = await DeliveryMeasure.countDocuments(query);
        
        return {delivery_measures, count};
    }
    ),
    getSingle: catchWrapDb(`${NAMESPACE}.getSingle`, async (data) => {
        const deliveryMeasure = await DeliveryMeasure.findOne({id: data.id});

        return deliveryMeasure;
    }),
    delete: catchWrapDb(`${NAMESPACE}.delete`, async(data) => {
        const deliveryMeasure = await DeliveryMeasure.deleteOne({id: data.id});

        return deliveryMeasure;
    }
    ),
};

module.exports = deliveryMeasureStore;
