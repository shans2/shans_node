const mongoose = require("mongoose");
const { v4 } = require("uuid");
var fns_format = require('date-fns/format');
var skipEmpty = require('mongoose-skip-empty');
var AutoIncrement = require('mongoose-sequence')(mongoose);

const CategorySchema = mongoose.Schema(
    {
        id: {
            type: String,
            default: v4,
            unique: true
        },
        num_id: {
            type: Number,
            set: skipEmpty,

        },
        name: {
            type: mongoose.Schema.Types.Mixed,
            required: [true, "Category must have name"],
            index: true,
        },
        status: {
            type: String,
            default: "active",
            index: true,
            enum: ['active', 'inactive'],
        },
        order: {
            type: Number,
            required: [true, "Category must have order"],
        },
        description: {
            type: mongoose.Schema.Types.Mixed,
        },
        meta: {
            type: mongoose.Schema.Types.Mixed,
        },
        photo: {
            type: String,
        },
        division_id: {
            type: String,
        },
        created_at: {
            type: String,
            set: skipEmpty,
        }, 
        updated_at: { 
            type: String, 
            set: skipEmpty,
        },
    },
    {
        toObject: { virtuals: true },
        toJSON: { virtuals: true },
    }
);


CategorySchema.virtual("division", {
    ref: "Division",
    localField: "division_id",
    foreignField: 'id',
    justOne: true
})

CategorySchema.plugin(AutoIncrement, {id: 'category_num_id', inc_field: 'num_id'});


CategorySchema.pre('save', function(next){
    now = new Date();
    now_string = fns_format(now, 'dd.MM.yyyy HH:mm')
    this.updated_at = fns_format(now, 'dd.MM.yyyy HH:mm')

    if ( !this.created_at ) {
      this.created_at = fns_format(now, 'dd.MM.yyyy HH:mm')
    }

    next();
});

CategorySchema.pre('updateOne', function(next){
    now = new Date();
    this.update({updated_at:fns_format(now, 'dd.MM.yyyy HH:mm')})

    next();
});

module.exports = mongoose.model("Category", CategorySchema);
