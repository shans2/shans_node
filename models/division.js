const mongoose = require("mongoose");
const { v4 } = require("uuid");
var fns_format = require('date-fns/format');
var skipEmpty = require('mongoose-skip-empty');
var AutoIncrement = require('mongoose-sequence')(mongoose);


const DivisionSchema = mongoose.Schema(
    {
        id: {
            type: String,
            default: v4,
            unique: true
        },
        num_id: {
            type: Number,
            set: skipEmpty,

        },
        name: {
            type: mongoose.Schema.Types.Mixed,
            required: [true, "Division must have name"],
            index: true,
        },
        status: {
            type: String,
            default: "active",
            index: true,
            enum: ['active', 'inactive'],
        },
        order: {
            type: Number,
            required: [true, "Division must have order"],
        },
        description: {
            type: mongoose.Schema.Types.Mixed,
        },
        meta: {
            type: mongoose.Schema.Types.Mixed,
        },
        icon: {
            type: String,
        },
        photo: {
            type: String,
        },
        background_colour: {
            type: String,
        },
        font_colour: {
            type: String,
        },
        created_at: {
            type: String,
            set: skipEmpty,
        }, 
        updated_at: { 
            type: String, 
            set: skipEmpty,
        }
    },
    {
        toObject: { virtuals: true },
        toJSON: { virtuals: true },
    }
);

DivisionSchema.plugin(AutoIncrement, {id: 'division_num_id', inc_field: 'num_id'});

DivisionSchema.pre('save', function(next){
    now = new Date();
    now_string = fns_format(now, 'dd.MM.yyyy HH:mm')
    this.updated_at = fns_format(now, 'dd.MM.yyyy HH:mm')

    if ( !this.created_at ) {
      this.created_at = fns_format(now, 'dd.MM.yyyy HH:mm')
    }

    next();
  });

DivisionSchema.pre('updateOne', function(next){
    now = new Date();
    this.update({updated_at:fns_format(now, 'dd.MM.yyyy HH:mm')})

    next();
});

module.exports = mongoose.model("Division", DivisionSchema);
