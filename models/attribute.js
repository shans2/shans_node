const mongoose = require("mongoose");
const { v4 } = require("uuid");
var fns_format = require('date-fns/format');
var skipEmpty = require('mongoose-skip-empty');
var AutoIncrement = require('mongoose-sequence')(mongoose);


const FilterSchema = mongoose.Schema(
    {
        id: {
            type: String,
            default: v4,
            unique: true
        },
        num_id: {
            type: Number,
            set: skipEmpty,

        },
        group_id: {
            type: String, 
        },
        blocks: {
            type: mongoose.Schema.Types.Mixed,
            index: true,
        },
        created_at: {
            type: String,
            set: skipEmpty,
        }, 
        updated_at: { 
            type: String, 
            set: skipEmpty,
        }
    },
    {
        toObject: { virtuals: true },
        toJSON: { virtuals: true },
    }
);

FilterSchema.plugin(AutoIncrement, {id: 'filter_num_id', inc_field: 'num_id'});

FilterSchema.virtual("group", {
    ref: "Group",
    localField: "group_id",
    foreignField: 'id',
    justOne: true
})


FilterSchema.pre('save', function(next){
    now = new Date();
    now_string = fns_format(now, 'dd.MM.yyyy HH:mm')
    this.updated_at = fns_format(now, 'dd.MM.yyyy HH:mm')

    if ( !this.created_at ) {
      this.created_at = fns_format(now, 'dd.MM.yyyy HH:mm')
    }

    next();
});

FilterSchema.pre('updateOne', function(next){
    now = new Date();
    this.update({updated_at:fns_format(now, 'dd.MM.yyyy HH:mm')})

    next();
});

module.exports = mongoose.model("Filter", FilterSchema);
