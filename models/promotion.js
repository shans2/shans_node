const mongoose = require("mongoose");
const { v4 } = require("uuid");
var fns_format = require('date-fns/format');
var skipEmpty = require('mongoose-skip-empty');
var AutoIncrement = require('mongoose-sequence')(mongoose);

const PromotionSchema = mongoose.Schema(
    {
        id: {
            type: String,
            default: v4,
            unique: true
        },
        num_id: {
            type: Number,
            set: skipEmpty,
        },
        name: {
            type: mongoose.Schema.Types.Mixed,
            required: [true, "Ribbon must have name"],
            index: true,
        },
        status: {
            type: String,
            default: "active",
            index: true,
            enum: ['active', 'inactive'],
        },
        show_timer: {
            type: String,
            default: "active",
            index: true,
            enum: ['active', 'inactive'],
        },
        order: {
            type: Number,
            index: true,
        },
        start_date: {
            type: String,
        },
        end_date: {
            type: String,
        },
        inside_image_url: {
            type: mongoose.Schema.Types.Mixed,
        },
        outside_image_url: {
            type: mongoose.Schema.Types.Mixed,
        },
        text: {
            type: mongoose.Schema.Types.Mixed,
        },
        link: {
            type: mongoose.Schema.Types.Mixed,
        },
        meta: {
            type: mongoose.Schema.Types.Mixed,
        },
        created_at: {
            type: String,
            set: skipEmpty,
        }, 
        updated_at: { 
            type: String, 
            set: skipEmpty,
        },
    },
    {
        toObject: { virtuals: true },
        toJSON: { virtuals: true },
    }
);

PromotionSchema.plugin(AutoIncrement, {id: 'promotion_num_id', inc_field: 'num_id'});

PromotionSchema.pre('save', function(next){
    now = new Date();
    now_string = fns_format(now, 'dd.MM.yyyy HH:mm')
    this.updated_at = fns_format(now, 'dd.MM.yyyy HH:mm')

    if ( !this.created_at ) {
      this.created_at = fns_format(now, 'dd.MM.yyyy HH:mm')
    }
    
    next();
});

PromotionSchema.pre('updateOne', function(next){
    now = new Date();
    this.update({updated_at:fns_format(now, 'dd.MM.yyyy HH:mm')})

    next();
});

module.exports = mongoose.model("Promotion", PromotionSchema);
