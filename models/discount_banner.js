const mongoose = require("mongoose");
const { v4 } = require("uuid");
var fns_format = require('date-fns/format');
var skipEmpty = require('mongoose-skip-empty');
var AutoIncrement = require('mongoose-sequence')(mongoose);

const DiscountBannerSchema = mongoose.Schema(
    {
        id: {
            type: String,
            default: v4,
            unique: true
        },
        num_id: {
            type: Number,
            set: skipEmpty,
        },
        name: {
            type: mongoose.Schema.Types.Mixed,
            required: [true, "DiscountBanner must have name"],
            index: true,
        },
        status: {
            type: String,
            default: "active",
            index: true,
            enum: ['active', 'inactive'],
        },
        inside: {
            type: mongoose.Schema.Types.Mixed,
            required: [true, "DiscountBanner must have inside"],
            index: true,
        },
        meta: {
            type: mongoose.Schema.Types.Mixed,
            required: [true, "DiscountBanner must have meta"],
            index: true,
        },
        image_url: {
            type: mongoose.Schema.Types.Mixed,
        },
        outside_image: {
            type: mongoose.Schema.Types.Mixed,
        },
        start_date: {
            type: String,
        },
        end_date: {
            type: String,
        },
        order: {
            type: Number,
        },
        created_at: {
            type: String,
            set: skipEmpty,
        }, 
        updated_at: { 
            type: String, 
            set: skipEmpty,
        },
    },
    {
        toObject: { virtuals: true },
        toJSON: { virtuals: true },
    }
);

DiscountBannerSchema.plugin(AutoIncrement, {id: 'discount_banner_num_id', inc_field: 'num_id'});

DiscountBannerSchema.pre('save', function(next){
    now = new Date();
    now_string = fns_format(now, 'dd.MM.yyyy HH:mm')
    this.updated_at = fns_format(now, 'dd.MM.yyyy HH:mm')

    if ( !this.created_at ) {
      this.created_at = fns_format(now, 'dd.MM.yyyy HH:mm')
    }
    
    next();
});

DiscountBannerSchema.pre('updateOne', function(next){
    now = new Date();
    this.update({updated_at:fns_format(now, 'dd.MM.yyyy HH:mm')})

    next();
});

module.exports = mongoose.model("DiscountBanner", DiscountBannerSchema);