const mongoose = require("mongoose");
const { v4 } = require("uuid");
var fns_format = require('date-fns/format');
var skipEmpty = require('mongoose-skip-empty');
var AutoIncrement = require('mongoose-sequence')(mongoose);


const BranchSchema = mongoose.Schema(
    {
        id: {
            type: String,
            default: v4,
            unique: true
        },
        num_id: {
            type: Number,
            set: skipEmpty,
        },
        name: {
            type: mongoose.Schema.Types.Mixed,
            required: [true, "Branch must have name"],
        },
        description: {
            type: mongoose.Schema.Types.Mixed,
            required: [true, "Branch must have description"],
        },
        meta: {
            type: mongoose.Schema.Types.Mixed,
        },
        phone_number: {
            type: String,
        },
        manager_name: {
            type: String,
        },
        photo_url: {
            type: String,
        },
        status: {
            type: String,
            default: "active",
            index: true,
            enum: ['active', 'inactive'],
        },
        latitude: {
            type: String,
        },
        longitude: {
            type: String,
        },
        region: {
            type: String,
        },
        created_at: {
            type: String,
            set: skipEmpty,
        }, 
        updated_at: { 
            type: String, 
            set: skipEmpty,
        }
    },
    {
        toObject: { virtuals: true },
        toJSON: { virtuals: true },
    }
);

BranchSchema.plugin(AutoIncrement, {id: 'branch_num_id', inc_field: 'num_id'});

BranchSchema.pre('save', function(next){
    now = new Date();
    now_string = fns_format(now, 'dd.MM.yyyy HH:mm')
    this.updated_at = fns_format(now, 'dd.MM.yyyy HH:mm')

    if ( !this.created_at ) {
      this.created_at = fns_format(now, 'dd.MM.yyyy HH:mm')
    }

    next();
  });

BranchSchema.pre('updateOne', function(next){
    now = new Date();
    this.update({updated_at:fns_format(now, 'dd.MM.yyyy HH:mm')})

    next();
});

module.exports = mongoose.model("Branch", BranchSchema);
