const mongoose = require("mongoose");
const { v4 } = require("uuid");
var fns_format = require('date-fns/format');
var skipEmpty = require('mongoose-skip-empty');
var AutoIncrement = require('mongoose-sequence')(mongoose);


const DeliveryMeasureSchema = mongoose.Schema(
    {
        id: {
            type: String,
            default: v4,
            unique: true
        },
        num_id: {
            type: Number,
            set: skipEmpty,

        },
        region: {
            type: mongoose.Schema.Types.Mixed,
            required: [true, "Delivery measure must have name"],
            index: true,
        },
        status: {
            type: String,
            default: "active",
            index: true,
            enum: ['active', 'inactive'],
        },
        from_kg: {
            type: Number,
            required: [true, "Delivery measure must have from_kg"],
        },
        to_kg: {
            type: Number,
            required: [true, "Delivery measure must have to_kg"],
        },
        price: {
            type: Number,
            required: [true, "Delivery measure must have price"],
        },
        created_at: {
            type: String,
            set: skipEmpty,
        }, 
        updated_at: { 
            type: String, 
            set: skipEmpty,
        }
    },
    {
        toObject: { virtuals: true },
        toJSON: { virtuals: true },
    }
);

DeliveryMeasureSchema.plugin(AutoIncrement, {id: 'delivery_measure_num_id', inc_field: 'num_id'});

DeliveryMeasureSchema.pre('save', function(next){
    now = new Date();
    now_string = fns_format(now, 'dd.MM.yyyy HH:mm')
    this.updated_at = fns_format(now, 'dd.MM.yyyy HH:mm')

    if ( !this.created_at ) {
      this.created_at = fns_format(now, 'dd.MM.yyyy HH:mm')
    }

    next();
  });

DeliveryMeasureSchema.pre('updateOne', function(next){
    now = new Date();
    this.update({updated_at:fns_format(now, 'dd.MM.yyyy HH:mm')})

    next();
});

module.exports = mongoose.model("Delivery_Measure", DeliveryMeasureSchema);
